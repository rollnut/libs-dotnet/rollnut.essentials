namespace RollNut.Essentials
{
    /// <summary>
    /// Each exception thrown by RollNut inerhits from <see cref="NutException"/>.
    /// </summary>
    public class NutException : System.Exception
    {
        public NutException() { }
        public NutException(string message) : base(message) { }
        public NutException(string message, System.Exception inner) : base(message, inner) { }
    }
}