﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RollNut.Essentials.Helper
{
    /// <summary>
    /// Essential helper for calculating.
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// Get the median for given numbers.
        /// </summary>
        /// <param name="source">All source numbers to calculate the median from.</param>
        /// <exception cref="InvalidOperationException">Source contains no elements.</exception>
        public static double Median(IEnumerable<double> source)
        {
            var decimals = source.Select(o => Convert.ToDecimal(o));
            return (double)Median(decimals);
        }

        /// <summary>
        /// Get the median for given numbers.
        /// </summary>
        /// <param name="source">All source numbers to calculate the median from.</param>
        /// <exception cref="InvalidOperationException">Source contains no elements.</exception>
        public static decimal Median(IEnumerable<decimal> source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (!source.Any())
            {
                throw new InvalidOperationException("Source contains no elements.");
            }

            // Copy the given numbers and sort them.
            var orderedNumbers = source
                .OrderBy(o => o)
                .ToArray();                
            
            int count = orderedNumbers.Length;
            
            // If count is even then return the average of the two middle values.
            if (count % 2 == 0)
            {
                decimal a = orderedNumbers[count / 2 - 1];
                decimal b = orderedNumbers[count / 2];
                return (a + b) / 2m;
            }
            // If count is odd return the middle value.
            else
            {
                return orderedNumbers[count / 2];
            }
        }

        /// <summary>
        /// Check if addittion will overflow.
        /// <para>
        /// License: https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License
        /// </para>
        /// <para>
        /// Author: https://www.programming-idioms.org/idiom/85/check-if-integer-addition-will-overflow/3637/csharp"
        /// </para>
        /// </summary>
        public static bool AddingWillOverflow(int x, int y)
        {
            bool willOverflow = false;

            if (x > 0 && y > 0)
            {
                if (y > (int.MaxValue - x)) 
                {
                    willOverflow = true;
                }
            }

            if (x < 0 && y < 0)
            {
                if (y < (int.MinValue - x))
                { 
                    willOverflow = true;
                }
            }

            return willOverflow;
        }
    }
}