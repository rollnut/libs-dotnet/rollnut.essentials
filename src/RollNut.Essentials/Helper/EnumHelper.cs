using System;
using System.Collections.Generic;
using System.Linq;

namespace RollNut.Essentials.Helper
{
    /// <summary>
    /// Essential helper for Enum.
    /// </summary>
    public class EnumHelper
    {
        #region Has, HasAny, Merge, Remove, Invert

        /// <summary>
        /// Return true if all given flags (right) are contained in (left) value (return false if left or right enum is zero).
        /// </summary>
        /// <param name="value">The value to check (if zero then return false).</param>
        /// <param name="flags">The flags to check if contained (if zero then return false).</param>
        public static bool HasFlag(Enum value, params Enum[] flags)
        {
            var valueNumber = Convert.ToInt64(value);
            if (valueNumber == 0) return false;

            var flagsNumber = ToNumber64(flags);
            if (flagsNumber == 0) return false;

            return (valueNumber & flagsNumber) == flagsNumber;
        }

        /// <summary>
        /// Return true if any given flag (right) is contained in (left) value (return false if left or right enum is zero).
        /// </summary>
        /// <param name="value">The value to check (if zero then return false).</param>
        /// <param name="flags">The flags to check if contained (if zero then return false).</param>
        public static bool HasAnyFlag(Enum value, params Enum[] flags)
        {
            var valueNumber = Convert.ToInt64(value);
            if (valueNumber == 0) return false;

            var flagsNumber = ToNumber64(flags);
            if (flagsNumber == 0) return false;

            return (valueNumber & flagsNumber) != 0;
        }

        /// <summary>
        /// Merge given flags into one. Zero values are ignored (except if both sides are zero).
        /// </summary>
        /// <param name="value">Enum to merge.</param>
        /// <param name="flags">Enum(s) to merge.</param>
        public static Enum MergeFlag(Enum value, params Enum[] flags)
        {
            var valueNumber = Convert.ToInt64(value);
            var flagsNumber = ToNumber64(flags);

            var merged = Enum.ToObject(value.GetType(), (valueNumber | flagsNumber));
            return (Enum)merged;
        }

        /// <summary>
        /// Remove given flags from value. Zero values are ignored.
        /// </summary>
        /// <param name="value">Enum to remove on.</param>
        /// <param name="flags">Enum(s) to remove.</param>
        public static Enum RemoveFlag(Enum value, params Enum[] flags)
        {
            var valueNumber = Convert.ToInt64(value);
            var flagsNumber = ToNumber64(flags);

            var removed = Enum.ToObject(value.GetType(), (valueNumber & ~flagsNumber));
            return (Enum)removed;
        }

        /// <summary>
        /// Invert given flags from value. Zero value on right side is ignored.
        /// </summary>
        /// <param name="value">Enum to invert on.</param>
        /// <param name="flags">Enum(s) to invert.</param>
        public static Enum InvertFlag(Enum value, params Enum[] flags)
        {
            var valueNumber = Convert.ToInt64(value);
            var flagsNumber = ToNumber64(flags);

            var toggle = Enum.ToObject(value.GetType(), (valueNumber ^= flagsNumber));
            return (Enum)toggle;
        }

        #endregion


        // .:: Converter

        #region Converter - ToNumber

        /// <summary>
        /// Convert given enum list to an number.
        /// Duplicate values and the zero one will be lost.
        /// </summary>
        /// <param name="flags">The flags to convert (duplicate values and the zero one will be lost).</param>
        public static Int64 ToNumber64(params Enum[] flags)
        {
            var flagNumbers = flags.Select(x => Convert.ToInt64(x));
            return ToNumber64(flagNumbers.ToArray());
        }
        /// <summary>
        /// Convert given number list to a merged number (bitwise).
        /// Duplicate values and the zero one will be lost.
        /// </summary>
        /// <param name="flags">The flags to convert (duplicate values and the zero one will be lost).</param>
        public static Int64 ToNumber64(params Int64[] flags)
        {
            Int64 resultNumber = 0;
            foreach (var flag in flags)
            {
                resultNumber |= flag;
            }
            return resultNumber;
        }

        #endregion

        #region Converter - ToList

        /// <summary>
        /// Convert given enum to a list of all flag instances (ToString()-Method is used).
        /// </summary>
        /// <param name="enumType">The enum type.</param>
        /// <param name="flaggedEnum">The enum to convert to list. The ToString()-Method is used (should be flagged).</param>
        public static IEnumerable<Enum> ToList(Type enumType, Enum flaggedEnum)
        {
            //if (!enumType.IsEnum) throw new ArgumentException(string.Format("Given enumType must be an enum type ({0}).", enumType.Name), "enumType");

            var enumValues = flaggedEnum.ToString();
            var splitted = enumValues.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
            var flagList = splitted.Select(x => (Enum)Enum.Parse(enumType, x, true));
            return flagList;
        }

        /// <summary>
        /// Convert given enum to a list of all flag instances (ToString()-Method is used).
        /// </summary>
        /// <param name="enumType">The enum type.</param>
        /// <param name="flaggedEnum">The enum to convert to list. The ToString()-Method is used (should be flagged).</param>
        public static IEnumerable<T> ToList<T>(T flaggedEnum)
            where T : struct, IComparable, IFormattable
        {
            var typeofT = typeof(T);
            //if (!typeofT.IsEnum) throw new ArgumentException(string.Format("The generic T must be an enum type ({0}).", typeofT.Name), "T");

            return ToList(typeofT, (Enum)(object)flaggedEnum).Select(x => (T)(object)x);
        }

        #endregion

        #region Converter - (List)ToEnum

        /// <summary>
        /// Convert given enum list to an merged single flagged enum instance.
        /// Duplicate values and the zero one will be lost.
        /// </summary>
        /// <param name="flags">The list with the flags (duplicate values and the zero one will be lost).</param>
        public static Enum ToEnum(Type enumType, params Enum[] flags)
        {
            if (enumType == null) throw new ArgumentNullException("enumType");
            //if (!enumType.IsEnum) throw new ArgumentException(string.Format("The enumType must be an enum type ({0}).", enumType.Name), "enumType");

            if (flags == null || flags.Length == 0) throw new ArgumentException("Can not be null or empty.", "flags");
            if (!flags.All(x => x.GetType().Equals(enumType))) throw new ArgumentException(string.Format("Each given flag must be from type '{0}'.", enumType.Name), "flags");

            var number = ToNumber64(flags);
            return (Enum)Enum.ToObject(enumType, number);
        }

        /// <summary>
        /// Convert given enum list to an merged single flagged enum instance.
        /// Duplicate values and the zero one will be lost.
        /// </summary>
        /// <typeparam name="T">The enum type.</typeparam>
        /// <param name="flags">The list with the flags (duplicate values and the zero one will be lost).</param>
        public static T ToEnum<T>(IEnumerable<T> flags)
            where T : struct, IComparable, IFormattable
        {
            var typeofT = typeof(T);
            //if (!typeofT.IsEnum) throw new ArgumentException(string.Format("The generic T must be an enum type ({0}).", typeofT.Name), "T");
            if (flags == null || !flags.Any()) throw new ArgumentException("flags");

            var resultEnum = ToEnum(typeofT, flags.Select(x => (Enum)(object)x).ToArray());
            return (T)(object)resultEnum;
        }

        #endregion
    }
}