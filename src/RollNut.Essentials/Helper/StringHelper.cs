﻿using System;
using System.Text.RegularExpressions;

namespace RollNut.Essentials.Helper
{
    /// <summary>
    /// Essential helper for String.
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Return true if given string represents a valid email address.
        /// </summary>
        /// <param name="value">The email string to validate.</param>
        public static bool IsValidEmailAddress(string value)
        {
            var regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");

            // Maybee rule below is more better than above one.
            //Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,25})+)$");

            return regex.IsMatch(value);
        }

        /// <summary>
        /// Returns true if given string represents a well formatted <see cref="Guid"/>.
        /// </summary>
        public static bool IsValidGuid(string value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));

            // see: https://connect.microsoft.com/VisualStudio/feedback/ViewFeedback.aspx?FeedbackID=94072&wa=wsignin1.0#tabs
            var format = new Regex(
                "^[A-Fa-f0-9]{32}$|" +
                "^({|\\()?[A-Fa-f0-9]{8}-([A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}(}|\\))?$|" +
                "^({)?[0xA-Fa-f0-9]{3,10}(, {0,1}[0xA-Fa-f0-9]{3,6}){2}, {0,1}({)([0xA-Fa-f0-9]{3,4}, {0,1}){7}[0xA-Fa-f0-9]{3,4}(}})$");
            Match match = format.Match(value);
            
            return match.Success;
        }

        /// <summary>
        /// Removes all non-ascii characters and return it.
        /// </summary>
        /// <param name="value">The string to strip.</param>
        public static string StripToAscii(string value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));
            return Regex.Replace(value, @"[^\u0000-\u007F]", string.Empty);
        }

        /// <summary>
        /// Remove xml tags and return only surrounding parts and their content.
        /// </summary>
        /// <param name="value">The string to remove xml tags.</param>
        /// <param name="replacementText">Replace text for each opening and closing tag.</param>
        public static string StripXml(string value, string replacementText = "")
        {
            if (value == null) throw new ArgumentNullException(nameof(value));

            var tagsExpression = new Regex(@"</?.+?>");
            return tagsExpression.Replace(value, replacementText);
        }

        //public static string Strip(string value)
        //{
        //    char.IsUpper
        //    CharType
        //}

        //[Flags]
        //public enum CharTypes
        //{
        //    Letter = 0,
        //    LowerLetter = 1,
        //    UpperLetter = 2,
        //    Number = 4,
        //    Digit = 8,
        //    SpecialChar = 16,
        //    WhiteSpace,
        //    Separator,
        //    Punctuation,
        //    Symbol,
        //}
    }
}