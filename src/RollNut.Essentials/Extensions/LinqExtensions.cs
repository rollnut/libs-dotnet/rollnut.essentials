using System;
using System.Collections.Generic;
using System.Linq;
using RollNut.Essentials.Helper;

namespace RollNut.Essentials.Extensions
{
    /// <summary>
    /// Essential extensions for Linq (IEnumerable and ICollection).
    /// </summary>
    public static class LinqExtensions
    {
        #region MinOrDefault()

        public static decimal? MinOrDefault(this IEnumerable<decimal?> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static decimal MinOrDefault(this IEnumerable<decimal> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static double? MinOrDefault(this IEnumerable<double?> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static double MinOrDefault(this IEnumerable<double> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static float? MinOrDefault(this IEnumerable<float?> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static float MinOrDefault(this IEnumerable<float> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static int? MinOrDefault(this IEnumerable<int?> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static int MinOrDefault(this IEnumerable<int> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static long? MinOrDefault(this IEnumerable<long?> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static long MinOrDefault(this IEnumerable<long> source) { return source.Count() == 0 ? 0 : source.Min(); }
        public static TSource MinOrDefault<TSource>(this IEnumerable<TSource> source) { return source.Count() == 0 ? default(TSource) : source.Min(); }
        public static decimal? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static decimal MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static double? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static double MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static float? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static float MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static int? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static int MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static long? MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static long MinOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector) { return source.Count() == 0 ? 0 : source.Min(selector); }
        public static TResult MinOrDefault<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector) { return source.Count() == 0 ? default(TResult) : MinOrDefault<TSource, TResult>(source, selector); }

        #endregion

        #region MinOrDefault(fallback)

        public static decimal? MinOrDefault(this IEnumerable<decimal?> source, decimal? fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static decimal MinOrDefault(this IEnumerable<decimal> source, decimal fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static double? MinOrDefault(this IEnumerable<double?> source, double? fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static double MinOrDefault(this IEnumerable<double> source, double fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static float? MinOrDefault(this IEnumerable<float?> source, float? fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static float MinOrDefault(this IEnumerable<float> source, float fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static int? MinOrDefault(this IEnumerable<int?> source, int? fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static int MinOrDefault(this IEnumerable<int> source, int fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static long? MinOrDefault(this IEnumerable<long?> source, long? fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static long MinOrDefault(this IEnumerable<long> source, long fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static TSource MinOrDefault<TSource>(this IEnumerable<TSource> source, TSource fallback) { return source.Count() == 0 ? fallback : source.Min(); }
        public static decimal? MinOrDefault<TSource>(this IEnumerable<TSource> source, decimal? fallback, Func<TSource, decimal?> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static decimal MinOrDefault<TSource>(this IEnumerable<TSource> source, decimal fallback, Func<TSource, decimal> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static double? MinOrDefault<TSource>(this IEnumerable<TSource> source, double? fallback, Func<TSource, double?> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static double MinOrDefault<TSource>(this IEnumerable<TSource> source, double fallback, Func<TSource, double> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static float? MinOrDefault<TSource>(this IEnumerable<TSource> source, float? fallback, Func<TSource, float?> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static float MinOrDefault<TSource>(this IEnumerable<TSource> source, float fallback, Func<TSource, float> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static int? MinOrDefault<TSource>(this IEnumerable<TSource> source, int? fallback, Func<TSource, int?> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static int MinOrDefault<TSource>(this IEnumerable<TSource> source, int fallback, Func<TSource, int> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static long? MinOrDefault<TSource>(this IEnumerable<TSource> source, long? fallback, Func<TSource, long?> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static long MinOrDefault<TSource>(this IEnumerable<TSource> source, long fallback, Func<TSource, long> selector) { return source.Count() == 0 ? fallback : source.Min(selector); }
        public static TResult MinOrDefault<TSource, TResult>(this IEnumerable<TSource> source, TResult fallback, Func<TSource, TResult> selector) { return source.Count() == 0 ? fallback : MinOrDefault<TSource, TResult>(source, selector); }

        #endregion

        #region MaxOrDefault()

        public static decimal? MaxOrDefault(this IEnumerable<decimal?> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static decimal MaxOrDefault(this IEnumerable<decimal> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static double? MaxOrDefault(this IEnumerable<double?> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static double MaxOrDefault(this IEnumerable<double> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static float? MaxOrDefault(this IEnumerable<float?> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static float MaxOrDefault(this IEnumerable<float> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static int? MaxOrDefault(this IEnumerable<int?> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static int MaxOrDefault(this IEnumerable<int> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static long? MaxOrDefault(this IEnumerable<long?> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static long MaxOrDefault(this IEnumerable<long> source) { return source.Count() == 0 ? 0 : source.Max(); }
        public static TSource MaxOrDefault<TSource>(this IEnumerable<TSource> source) { return source.Count() == 0 ? default(TSource) : source.Max(); }
        public static decimal? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static decimal MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static double? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static double MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static float? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static float MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static int? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static int MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static long? MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static long MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector) { return source.Count() == 0 ? 0 : source.Max(selector); }
        public static TResult MaxOrDefault<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector) { return source.Count() == 0 ? default(TResult) : MaxOrDefault<TSource, TResult>(source, selector); }

        #endregion

        #region MaxOrDefault(fallback)

        public static decimal? MaxOrDefault(this IEnumerable<decimal?> source, decimal? fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static decimal MaxOrDefault(this IEnumerable<decimal> source, decimal fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static double? MaxOrDefault(this IEnumerable<double?> source, double? fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static double MaxOrDefault(this IEnumerable<double> source, double fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static float? MaxOrDefault(this IEnumerable<float?> source, float? fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static float MaxOrDefault(this IEnumerable<float> source, float fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static int? MaxOrDefault(this IEnumerable<int?> source, int? fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static int MaxOrDefault(this IEnumerable<int> source, int fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static long? MaxOrDefault(this IEnumerable<long?> source, long? fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static long MaxOrDefault(this IEnumerable<long> source, long fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static TSource MaxOrDefault<TSource>(this IEnumerable<TSource> source, TSource fallback) { return source.Count() == 0 ? fallback : source.Max(); }
        public static decimal? MaxOrDefault<TSource>(this IEnumerable<TSource> source, decimal? fallback, Func<TSource, decimal?> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static decimal MaxOrDefault<TSource>(this IEnumerable<TSource> source, decimal fallback, Func<TSource, decimal> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static double? MaxOrDefault<TSource>(this IEnumerable<TSource> source, double? fallback, Func<TSource, double?> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static double MaxOrDefault<TSource>(this IEnumerable<TSource> source, double fallback, Func<TSource, double> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static float? MaxOrDefault<TSource>(this IEnumerable<TSource> source, float? fallback, Func<TSource, float?> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static float MaxOrDefault<TSource>(this IEnumerable<TSource> source, float fallback, Func<TSource, float> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static int? MaxOrDefault<TSource>(this IEnumerable<TSource> source, int? fallback, Func<TSource, int?> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static int MaxOrDefault<TSource>(this IEnumerable<TSource> source, int fallback, Func<TSource, int> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static long? MaxOrDefault<TSource>(this IEnumerable<TSource> source, long? fallback, Func<TSource, long?> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static long MaxOrDefault<TSource>(this IEnumerable<TSource> source, long fallback, Func<TSource, long> selector) { return source.Count() == 0 ? fallback : source.Max(selector); }
        public static TResult MaxOrDefault<TSource, TResult>(this IEnumerable<TSource> source, TResult fallback, Func<TSource, TResult> selector) { return source.Count() == 0 ? fallback : MaxOrDefault<TSource, TResult>(source, selector); }

        #endregion


        #region MinObjects() / MaxObjects()

        /// <summary>
        /// Return the objects which contains the minimum value.
        /// </summary>
        /// <remarks>
        /// TODO: The body of this method could be optimized for speed.
        /// Currently two iteration are done to find the min objects.
        /// </remarks>
        public static IEnumerable<T> MinObjects<T>(this IEnumerable<T> source, Func<T, object> selector)
        {
            // Get the min value from given selector (not the object).
            var minValue = source.Min(selector);
            // Find all objects which contains the detected minValue.
            var result = source.Where(o => object.Equals(minValue, selector.Invoke(o)));

            return result;
        }

        /// <summary>
        /// Return the objects which contains the maximum value.
        /// </summary>
        /// <remarks>
        /// TODO: The body of this method could be optimized for speed.
        /// Currently two iteration are done to find the max objects.
        /// </remarks>
        public static IEnumerable<T> MaxObjects<T>(this IEnumerable<T> source, Func<T, object> selector)
        {
            // Get the max value from given selector (not the object).
            var maxValue = source.Max(selector);
            // Find all objects which contains the detected maxValue.
            var result = source.Where(o => object.Equals(maxValue, selector.Invoke(o)));

            return result;
        }

        #endregion

        #region Median()

        // TODO: Implement the Median for different.

        //public static float? Median(this IEnumerable<float?> source);
        //public static double? Median(this IEnumerable<long?> source);
        //public static double? Median(this IEnumerable<int?> source);
        //public static double? Median(this IEnumerable<double?> source);
        //public static decimal? Median(this IEnumerable<decimal?> source);
        //public static float Median(this IEnumerable<float> source) { return (float)MathHelper.Median(source.OfType<decimal>()); }
        //public static double Median(this IEnumerable<long> source) { return (double)MathHelper.Median(source.OfType<decimal>()); }
        //public static double Median(this IEnumerable<int> source) { return (double)MathHelper.Median(source.OfType<decimal>()); }
        //public static double Median(this IEnumerable<double> source) { return MathHelper.Median(source); }
        public static decimal Median(this IEnumerable<decimal> source)
        {
            return MathHelper.Median(source);
        }
        public static decimal Median<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
        {
            return source.Select(selector).Median();
        }

        #endregion


        // .:: Queries (Except, Concat, NonIntersect)

        #region .:: Queries ::.

        // .:: Except

        /// <summary>
        /// Produces the set difference of two sequences by using the default equality
        /// comparer to compare values.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
        /// <param name="first">An System.Collections.Generic.IEnumerable<T> whose elements that are not also in second will be returned.</param>
        /// <param name="second">Return the sequence without that element.</param>
        public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, TSource second)
        {
            if (second == null) throw new ArgumentNullException(nameof(second));
            return first.Except(new TSource[] { second });
        }

        // .:: Concat

        /// <summary>
        /// Concatenates given item with sequence .
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of the input sequences</typeparam>
        /// <param name="first">The first sequence to concatenate.</param>
        /// <param name="second">The item to concat with first sequence.</param>
        /// <returns>An System.Collections.Generic.IEnumerable<T> that contains the concatenated
        /// elements of the input sequence and the item.</returns>
        public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> first, TSource second)
        {
            if (second == null) throw new ArgumentNullException(nameof(second));
            return first.Concat(new TSource[] { second });
        }


        // .:: NonIntersect

        /// <summary>
        /// Return all (non-distincted) items which are not contained in first and second sequence
        /// (use parameter 'onlyItemsFromFirst' if only items from first sequence are wanted).
        /// </summary>
        /// <param name="onlyItemsFromFirst">When false then the result of both squences are contained in result collection. When true then only items from first sequence.</param>
        public static IEnumerable<TSource> NonIntersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, bool onlyItemsFromFirst)
        {
            return NonIntersect<TSource>(first, second, null, onlyItemsFromFirst);
        }

        /// <summary>
        /// Return all (non-distincted) items which are not contained in first or in both sequences (use parameter 'onlyItemsFromFirst' to define the mode).
        /// </summary>
        /// <param name="onlyItemsFromFirst">When false then all items from first and second squence are contained. When true then only items from first sequence.</param>
        public static IEnumerable<TSource> NonIntersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer, bool onlyItemsFromFirst)
        {
            if (first == null) throw new ArgumentNullException(nameof(first));
            if (second == null) throw new ArgumentNullException(nameof(second));

            IEnumerable<TSource> result;

            // Call the original linq Intersect()-Method (with or without comparer).
            var intersectingItems = comparer == null
                ? first.Intersect(second)
                : first.Intersect(second, comparer);

            // The special logic for 'non'-intersect (use the Except()-Method from linq).
            if (onlyItemsFromFirst)
            {
                result = first.Except(intersectingItems);
            }
            else
            {
                result = first.Concat(second).Except(intersectingItems);
            }
            return result;
        }

        #endregion


        // .:: Non-Linq-Extensins

        #region IColleciton / IList (non-linq-extensions)

        /// <summary>
        /// Syncs the collection by given IEnumerable.
        /// Items gets removed from collection if not found in given IEnumerable.
        /// If items are in given IEnumerable but not in collection they are added.
        /// </summary>
        /// <param name="first">The collection to sync (this instance gets updated).</param>
        /// <param name="second">This values will be untouched.</param>
        public static void SyncFrom<TSource>(this ICollection<TSource> first, IEnumerable<TSource> second)
        {
            if (first == null) throw new ArgumentNullException(nameof(first));
            if (second == null) throw new ArgumentNullException(nameof(second));

            var currentItems = new HashSet<TSource>(first);

            // Remove
            {
                // Get the items which are contained in first but not in second list.
                var itemsToRemove = currentItems.NonIntersect(second, true);
                if (itemsToRemove.Any())
                {
                    // If all items should be removed then clear the collection directly.
                    if (itemsToRemove.Count() == currentItems.Count())
                    {
                        first.Clear();
                    }
                    // Else remove each item step by step.
                    else
                    {
                        foreach (var itemToRemove in itemsToRemove)
                        {
                            first.Remove(itemToRemove);
                        }
                    }                    
                }
            }
            // Add
            {
                // Get the items which not contained in first but in second list.
                var itemsToAdd = second.NonIntersect(currentItems, true);
                foreach (var itemToAdd in itemsToAdd)
                {
                    first.Add(itemToAdd);
                }
            }
        }

        #endregion

        #region IEnumerable (non-generic)

        /// <summary>
        /// Returns the number of elements in a sequence (watch out! Each item will be enumerated).
        /// </summary>
        /// <param name="source">A sequence that contains elements to be counted.</param>
        /// <returns>The number of elements in the input sequence.</returns>
        public static int Count(this System.Collections.IEnumerable source)
        {
            int count = 0;
            foreach (var item in source)
            {
                count++;
            }
            return count;
        }

        /// <summary>
        /// Determines whether a sequence contains any elements (watch out! The first item will be enumerated).
        /// </summary>
        /// <param name="source">The System.Collections.IEnumerable to check for emptiness.</param>
        /// <returns>True if the source sequence contains any elements; otherwise, false.</returns>
        public static bool Any(this System.Collections.IEnumerable source)
        {
            // Reset() must not be called on enumerator because GetEnumerator() return always a fresh instance.
            return source.GetEnumerator().MoveNext();
        }

        #endregion
    }
}
