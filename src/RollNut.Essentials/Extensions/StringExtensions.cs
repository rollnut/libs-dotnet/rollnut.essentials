using System;
using System.Text.RegularExpressions;
using RollNut.Essentials.Helper;

namespace RollNut.Essentials.Extensions
{
    /// <summary>
    /// Essential extensions for String.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Returns a new string that is equivalent to this string except for the removed characters.
        /// </summary>
        /// <param name="stringsToRemove">The strings to remove.</param>
        public static string Remove(this string value, params string[] stringsToRemove)
        {
            if (stringsToRemove == null) throw new ArgumentException(nameof(stringsToRemove));

            foreach (var valueToRemove in stringsToRemove)
            {
                if(!string.IsNullOrEmpty(valueToRemove))
                {
                    value = value.Replace(valueToRemove, "");
                }
            }
            return value;
        }

        /// <summary>
        /// Returns a new string that is equivalent to this string except for the removed characters.
        /// </summary>
        /// <param name="stringToRemove">The characters to be removed.</param>
        public static string Remove(this string value, string stringToRemove)
        {
            if (String.IsNullOrEmpty(stringToRemove)) throw new ArgumentException(nameof(stringToRemove));
            return value.Replace(stringToRemove, "");
        }

        /// <summary>
        /// Returns a new string that is equivalent to this string except for the replaced characters.
        /// </summary>
        /// <param name="stringToRemove">The string to be replaced with an empty string.</param>
        public static string Replace(this string value, string stringToRemove)
        {
            if (String.IsNullOrEmpty(stringToRemove)) throw new ArgumentException(nameof(stringToRemove));
            return value.Replace(stringToRemove, "");
        }

        /// <summary>
        /// Same as <see cref="string.Substring"/> except that no exception is thrown if length of given string is exceeded.
        /// <code>
        /// // Take first 5 characters:
        /// text.SubstringIronized(0, 5)
        /// 
        /// // Skip first 3 characters:
        /// text.SubstringIronized(3, int.MaxValue)
        /// </code>
        /// </summary>
        /// <param name="value">The text to substring.</param>
        /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
        /// <param name="length">The number of characters in the substring.</param>
        public static string SubstringIronized(this string value, int startIndex, int length)
        {
            if (startIndex >= value.Length) return "";
            
            if (MathHelper.AddingWillOverflow(startIndex, length)
                || startIndex + length >= value.Length)
            {
                length = value.Length - startIndex;
            }

            return value.Substring(startIndex, length);
        }

        /// <summary>
        /// Returns a new string but skip characters up to given count.
        /// Given count is allowed to be higher than string length (no exception is thrown).
        /// </summary>
        /// <param name="count">The number of chars to skip.</param>
        public static string SubstringSkip(this string value, int count)
        {
            return value.SubstringIronized(count, value.Length);
        }

        /// <summary>
        /// Returns a new string but only characters up to given count.
        /// Given count is allowed to be higher than string length (no exception is thrown).
        /// </summary>
        /// <param name="count">The number of chars to take.</param>
        public static string SubstringTake(this string value, int count)
        {
            return value.SubstringIronized(0, count);
        }

        /// <summary>
        /// Reverse the given string and return it.
        /// </summary>
        /// <param name="value">The string to reverse.</param>
        public static string Reverse(this string value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));

            char[] chars = value.ToCharArray();
            Array.Reverse(chars);
            return new String(chars);
        }

        /// <summary>
        /// Converts string to enum.
        /// </summary>
        /// <typeparam name="T">Type of enum.</typeparam>
        /// <param name="value">String value to convert into enum.</param>
        public static T ToEnum<T>(this string value)
            where T : struct
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        /// <summary>
        /// Splits a string into substrings that are based on given origin string.
        /// </summary>
        /// <param name="separator">
        /// A string that delimit the substrings in this string.
        /// </param>
        public static string[] Split(this string value, string separator)
        {
            if (separator is null) throw new ArgumentNullException(nameof(separator));
            return value.Split(new string[] { separator }, StringSplitOptions.None);
        }

        /// <summary>
        /// Splits a string into substrings that are based on given origin string.
        /// A parameter specifies whether to return empty array elements.
        /// </summary>
        /// <param name="separator">
        /// A string that delimit the substrings in this string.
        /// </param>
        /// <param name="options">
        /// System.StringSplitOptions.RemoveEmptyEntries to omit empty array elements
        /// from the array returned; or System.StringSplitOptions.None to include empty
        /// array elements in the array returned.
        /// </param>
        public static string[] Split(this string value, string separator, StringSplitOptions options)
        {
            if (separator is null) throw new ArgumentNullException(nameof(separator));
            return value.Split(new string[] { separator }, options);
        }
        
        /// <summary>
        /// Split string by camel cased rules using a white space as separator (e.g. MySuperClass -> My Super Class).
        /// </summary>
        /// <param name="value">The string to split by camel case rules.</param>
        public static string SplitCamelCase(this string value)
        {
            return SplitCamelCase(value, " ");
        }

        /// <summary>
        /// Split string by camel cased rules (e.g. MyClassName -> My Class Name).
        /// </summary>
        /// <param name="value">The string to split by camel case rules.</param>
        /// <param name="separator">The separator between the camel case splits.</param>
        public static string SplitCamelCase(this string value, string separator)
        {
            return Regex.Replace(value, @"(\B[A-Z]+?(?=[A-Z][^A-Z])|\B[A-Z]+?(?=[^A-Z]))", separator + "$1");
        }
    }
}