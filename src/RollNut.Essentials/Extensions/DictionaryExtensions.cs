using System;
using System.Collections.Generic;

namespace RollNut.Essentials.Extensions
{
    /// <summary>
    /// Essential extensions for Dictionary.
    /// </summary>
    public static class DictionaryExtensions
    {
        // Add

        /// <summary>
        /// Add given KeyValuePair to dictionary.
        /// </summary>
        public static void Add<TKey, TValue>(this Dictionary<TKey, TValue> dict, KeyValuePair<TKey, TValue> keyValue)
        {
            dict.Add(keyValue.Key, keyValue.Value);
        }

        /// <summary>
        /// Add given KeyValuePair's to dictionary.
        /// </summary>
        public static void AddRange<TKey, TValue>(this Dictionary<TKey, TValue> dict, IEnumerable<KeyValuePair<TKey, TValue>> itemsToAdd)
        {
            foreach (var item in itemsToAdd)
            {
                dict.Add(item.Key, item.Value);
            }
        }

        // Remove

        /// <summary>
        /// Removes all values for given keys.
        /// </summary>
        /// <param name="keys">The number of removed items (can be lower than given keys or equal).</param>
        /// <returns>Count of removed keys (remove only works if key was found in dictionary).</returns>
        public static int RemoveRange<TKey, TValue>(this Dictionary<TKey, TValue> dict, IEnumerable<TKey> keys)
        {
            int countOfRemovedKeys = 0;
            foreach (var key in keys)
            {
                if (dict.Remove(key))
                {
                    countOfRemovedKeys += 1;
                }
            }
            return countOfRemovedKeys;
        }

        // GetValueOrDefault

        /// <summary>
        /// Gets the value for given key. If no value found then return the default value for <paramkey>TValue</paramkey>.
        /// </summary>
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : default(TValue);
        }

        /// <summary>
        /// Gets the value for given key. If no value found then return given <paramkey>fallbackValue</paramkey>.
        /// </summary>
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue fallbackValue)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : fallbackValue;
        }

        /// <summary>
        /// Gets the value for given key. If no value found then return result of <paramkey>fallbackValueProvider</paramkey>.
        /// </summary>
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TValue> fallbackValueProvider)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : fallbackValueProvider();
        }
    }
}