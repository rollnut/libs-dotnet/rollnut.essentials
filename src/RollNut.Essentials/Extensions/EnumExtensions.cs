﻿using System;
using System.Linq;
using RollNut.Essentials.Helper;

namespace RollNut.Essentials.Extensions
{
    /// <summary>
    /// Essential extensions for Enum.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Return true if all given flags (right) are contained in (left) value (return false if left or right enum is zero).
        /// </summary>
        /// <param name="value">The value to check (if zero then return false).</param>
        /// <param name="flags">The flags to check if contained (if zero then return false).</param>
        public static bool HasFlag<T>(this T value, params T[] flags)
            where T : struct, IComparable, IFormattable
        {
            var leftSide = (Enum)(object)value;
            var rightSide = flags.Select(x => (Enum)(object)x).ToArray();
            return EnumHelper.HasFlag(leftSide, rightSide);
        }

        /// <summary>
        /// Return true if any given flag (right) is contained in (left) value (return false if left or right enum is zero).
        /// </summary>
        /// <param name="value">The value to check (if zero then return false).</param>
        /// <param name="flags">The flags to check if contained (if zero then return false).</param>
        public static bool HasAnyFlag<T>(this T value, params T[] flags)
            where T : struct, IComparable, IFormattable
        {
            var leftSide = (Enum)(object)value;
            var rightSide = flags.Select(x => (Enum)(object)x).ToArray();
            return EnumHelper.HasAnyFlag(leftSide, rightSide);
        }

        /// <summary>
        /// Merge given flags into one. Zero values are ignored (except if both sides are zero).
        /// </summary>
        /// <param name="value">Enum to merge.</param>
        /// <param name="flags">Enum(s) to merge.</param>
        public static T MergeFlag<T>(this T value, params T[] flags)
            where T : struct, IComparable, IFormattable
        {
            var leftSide = (Enum)(object)value;
            var rightSide = flags.Select(x => (Enum)(object)x).ToArray();
            return (T)(object)EnumHelper.MergeFlag(leftSide, rightSide);
        }

        /// <summary>
        /// Remove given flags from value. Zero values are ignored.
        /// </summary>
        /// <param name="value">Enum to remove on.</param>
        /// <param name="flags">Enum(s) to remove.</param>
        public static T RemoveFlag<T>(this T value, params T[] flags)
            where T : struct, IComparable, IFormattable
        {
            var leftSide = (Enum)(object)value;
            var rightSide = flags.Select(x => (Enum)(object)x).ToArray();
            return (T)(object)EnumHelper.RemoveFlag(leftSide, rightSide);
        }

        /// <summary>
        /// Invert given flags from value. Zero value on right side is ignored.
        /// </summary>
        /// <param name="value">Enum to invert on.</param>
        /// <param name="flags">Enum(s) to invert.</param>
        public static T InvertFlag<T>(this T value, params Enum[] flags)
            where T : struct, IComparable, IFormattable
        {
            var leftSide = (Enum)(object)value;
            var rightSide = flags.Select(x => (Enum)(object)x).ToArray();
            return (T)(object)EnumHelper.InvertFlag(leftSide, rightSide);
        }
    }
}