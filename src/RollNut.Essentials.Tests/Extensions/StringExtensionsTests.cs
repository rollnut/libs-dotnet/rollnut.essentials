using RollNut.Essentials.Extensions;
using Xunit;

namespace RollNut.Essentials.Tests.Extensions
{
    public class StringExtensionsTests
    {
        public class SubstringTests
        {
            [Theory]
            [InlineData(0, 6, "012345")]
            [InlineData(0, 5, "01234")]
            [InlineData(0, 4, "0123")]
            [InlineData(0, 3, "012")]
            [InlineData(0, 2, "01")]
            [InlineData(0, 1, "0")]
            [InlineData(0, 0, "")]
            [InlineData(6, 0, "")]
            [InlineData(5, 1, "5")]
            [InlineData(4, 2, "45")]
            [InlineData(3, 3, "345")]
            [InlineData(2, 4, "2345")]
            [InlineData(1, 5, "12345")]
            [InlineData(2, 3, "234")]
            [InlineData(4, 1, "4")]
            [InlineData(3, 0, "")]
            [InlineData(1, 4, "1234")]
            public void SubstringIronized_NormalPositiveSubstringBehaviour(int startIndex, int length, string expected)
            {
                var szenario = "012345";

                var actual = szenario.SubstringIronized(startIndex, length);

                Assert.Equal(expected, actual);
            }

            [Theory]
            [InlineData(0, 7, "012345")]
            [InlineData(0, 8, "012345")]
            [InlineData(0, 9, "012345")]
            [InlineData(1, 6, "12345")]
            [InlineData(1, int.MaxValue, "12345")]
            [InlineData(5, 5, "5")]
            public void SubstringIronized_RequestedLengthLongerThanText(int startIndex, int length, string expected)
            {
                var szenario = "012345";

                var actual = szenario.SubstringIronized(startIndex, length);

                Assert.Equal(expected, actual);
            }

            [Theory]
            [InlineData(6, 1, "")]
            [InlineData(7, 1, "")]
            [InlineData(8, 1, "")]
            [InlineData(6, 0, "")]
            [InlineData(7, 0, "")]
            public void SubstringIronized_RequestedStartIndexLongerThanText(int startIndex, int length, string expected)
            {
                var szenario = "012345";

                var actual = szenario.SubstringIronized(startIndex, length);

                Assert.Equal(expected, actual);
            }

            [Theory]
            [InlineData(0, "012345")]
            [InlineData(1, "12345")]
            [InlineData(2, "2345")]
            [InlineData(3, "345")]
            [InlineData(4, "45")]
            [InlineData(5, "5")]
            [InlineData(6, "")]
            [InlineData(7, "")]
            [InlineData(int.MaxValue, "")]
            public void SubstringSkip(int count, string expected)
            {
                var szenario = "012345";

                var actual = szenario.SubstringSkip(count);

                Assert.Equal(expected, actual);
            }

            [Theory]
            [InlineData(0, "")]
            [InlineData(1, "0")]
            [InlineData(2, "01")]
            [InlineData(3, "012")]
            [InlineData(4, "0123")]
            [InlineData(5, "01234")]
            [InlineData(6, "012345")]
            [InlineData(7, "012345")]
            [InlineData(int.MaxValue, "012345")]
            public void SubstringTake(int count, string expected)
            {
                var szenario = "012345";

                var actual = szenario.SubstringTake(count);

                Assert.Equal(expected, actual);
            }
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("camelCase", "camel Case")]
        [InlineData("CamelCase", "Camel Case")]
        [InlineData("camelCaseValue", "camel Case Value")]
        [InlineData("camelCASEValueText", "camel CASE Value Text")]
        [InlineData("FirstCamelValue secondValue ThirdCamelCase", "First Camel Value second Value Third Camel Case")]
        public void SplitCamelCase(string given, string expected)
        {
            var actual = given.SplitCamelCase();
            Assert.Equal(expected, actual);
        }
    }
}