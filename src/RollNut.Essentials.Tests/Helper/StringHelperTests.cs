using RollNut.Essentials.Helper;
using Xunit;

namespace RollNut.Essentials.Tests.Helper
{
    public class StringHelperTests
    {
        [Theory]
        [InlineData("johndoe@example.com")]
        [InlineData("john.doe@example.com")]
        [InlineData("johndoe@example.com.de")]
        public void IsValidEmailAddress_Valid(string given)
        {
            Assert.True(StringHelper.IsValidEmailAddress(given));
        }

        [Theory]
        [InlineData("johndoeexamplecom")]
        [InlineData("johndoeexample.com")]
        [InlineData("johndoe@examplecom")]
        [InlineData("@example.com")]
        [InlineData("johndoe@.com")]
        [InlineData("johndoe@example.")]
        public void IsValidEmailAddress_NotValid(string given)
        {
            Assert.False(StringHelper.IsValidEmailAddress(given));
        }

        [Theory]
        [InlineData("00000000-0000-0000-0000-000000000000")]
        [InlineData("{00000000-0000-0000-0000-000000000000}")]
        [InlineData("35da9838-9405-459c-91c5-5f084a0a2ffe")]
        [InlineData("{35da9838-9405-459c-91c5-5f084a0a2ffe}")]
        public void IsValidGuid_Valid(string given)
        {
            Assert.True(StringHelper.IsValidGuid(given));
        }

        [Theory]
        [InlineData("{{00000000-0000-0000-0000-000000000000}}")]
        [InlineData("{{35da9838-9405-459c-91c5-5f084a0a2ffe}}")]
        [InlineData("00000000-0000-0000-0000-00000000000")] // <- The last 0 is missing
        [InlineData("35da9838-9405-459c-91c5-5f084a0a2ff")]
        [InlineData("35da9838-459c-91c5-5f084a0a2ffe")]
        [InlineData("")]
        public void IsValidGuid_NotValid(string given)
        {
            Assert.False(StringHelper.IsValidGuid(given));
        }

        [Theory]
        [InlineData("hello world")]
        [InlineData("ABCDExyz123@")]
        [InlineData("héllö wörld", "hll wrld")]
        public void StripToAscii(string given, string expected = null)
        {
            if (expected == null)
            {
                expected = given;
            }

            var actual = StringHelper.StripToAscii(given);
            
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("<tag1>hello</tag1>", "|hello|")]
        [InlineData("<tag1>hello</tag1><tag2>world</tag2>", "|hello||world|")]
        [InlineData("<tag1>hello</tag1>greetings<tag2>world</tag2>", "|hello|greetings|world|")]
        public void StripXml(string given, string expected)
        {
            var actual = StringHelper.StripXml(given, "|");
            
            Assert.Equal(expected, actual);
        }
    }
}