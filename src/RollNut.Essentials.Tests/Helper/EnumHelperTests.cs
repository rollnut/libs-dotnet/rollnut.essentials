using Xunit;
using System;
using System.Linq;
using RollNut.Essentials.Helper;

namespace RollNut.Essentials.Tests.Helper
{    
    public class EnumHelperTests
    {
        public enum TestNonFlagEnum
        {
            None,
            Circle,
            Triangle,
            Quarter,
            Red,
            Green,
            Blue
        }
        [Flags]
        public enum TestFlagEnum
        {
            None = 0,
            Circle = 1,
            Triangle = 2,
            Quarter = 4,
            Red = 8,
            Green = 16,
            Blue = 32,
        }
        
        public class HasFlagMethod
        {
            // None Contained Tests

            [Fact]
            public void HasFlag_CheckNoneFlagWithNoneFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.None;
                var rightEnum= TestFlagEnum.None;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(false, result);
            }

            [Fact]
            public void HasFlag_CheckSingleFlagWithNoneFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter;
                var rightEnum = TestFlagEnum.None;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            [Fact]
            public void HasFlag_CheckNoneFlagWithSingleFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.None;
                var rightEnum = TestFlagEnum.Quarter;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }
          
            // Single Tests

            [Fact]
            public void HasFlag_CheckSingleFlagWithOtherSingleFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter;
                var rightEnum = TestFlagEnum.Triangle;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            [Fact]
            public void HasFlag_CheckSingleFlagWithSameSingleFlag_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter;
                var rightEnum = TestFlagEnum.Quarter;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }
         
            // Left Side Three Tests

            [Fact]
            public void HasFlag_CheckThreeFlagWithOtherSingleFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Triangle;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            [Fact]
            public void HasFlag_CheckThreeFlagWithContainedSingleFlag_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }
            
            [Fact]
            public void HasFlag_CheckThreeFlagWithContainedTwoFlag_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter | TestFlagEnum.Green;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }

            [Fact]
            public void HasFlag_CheckThreeFlagWithContainedSingleFlagAndNonContainedSingleFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter | TestFlagEnum.Blue;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            [Fact]
            public void HasFlag_CheckThreeFlagWithNonContainedTwoFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Circle | TestFlagEnum.Blue;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            [Fact]
            public void HasFlag_CheckThreeFlagWithSameThreeFlag_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }

            [Fact]
            public void HasFlag_CheckThreeFlagWithSameThreeFlagAndOneDifferent_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green | TestFlagEnum.Blue;

                bool result = EnumHelper.HasFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }
        }

        
        public class HasAnyFlagMethod
        {
            // None Contained Tests

            [Fact]
            public void HasAnyFlag_CheckNoneFlagWithNoneFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.None;
                var rightEnum = TestFlagEnum.None;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(false, result);
            }

            [Fact]
            public void HasAnyFlag_CheckSingleFlagWithNoneFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter;
                var rightEnum = TestFlagEnum.None;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            [Fact]
            public void HasAnyFlag_CheckNoneFlagWithSingleFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.None;
                var rightEnum = TestFlagEnum.Quarter;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            // Single Tests

            [Fact]
            public void HasAnyFlag_CheckSingleFlagWithOtherSingleFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter;
                var rightEnum = TestFlagEnum.Triangle;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            [Fact]
            public void HasAnyFlag_CheckSingleFlagWithSameSingleFlag_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter;
                var rightEnum = TestFlagEnum.Quarter;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }

            // Left Side Three Tests

            [Fact]
            public void HasAnyFlag_CheckThreeFlagWithOtherSingleFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Triangle;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            [Fact]
            public void HasAnyFlag_CheckThreeFlagWithContainedSingleFlag_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }

            [Fact]
            public void HasAnyFlag_CheckThreeFlagWithContainedTwoFlag_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter | TestFlagEnum.Green;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }

            [Fact]
            public void HasAnyFlag_CheckThreeFlagWithContainedSingleFlagAndNonContainedSingleFlag_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter | TestFlagEnum.Blue;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }

            [Fact]
            public void HasAnyFlag_CheckThreeFlagWithNonContainedTwoFlag_ReturnFalse()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Circle | TestFlagEnum.Blue;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, false);
            }

            [Fact]
            public void HasAnyFlag_CheckThreeFlagWithSameThreeFlag_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }

            [Fact]
            public void HasAnyFlag_CheckThreeFlagWithSameThreeFlagAndOneDifferent_ReturnTrue()
            {
                var leftEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var rightEnum = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green | TestFlagEnum.Blue;

                bool result = EnumHelper.HasAnyFlag(leftEnum, rightEnum);

                Assert.Equal(result, true);
            }
        }
        
        
        public class MergeFlagMethod
        {
            // None Value Tests

            [Fact]
            public void MergeFlag_NoneWithNone_NoMerging()
            {
                var expectedResult = TestFlagEnum.None;
                var mergeEnum1 = TestFlagEnum.None;
                var mergeEnum2 = TestFlagEnum.None;

                var result = (TestFlagEnum)(EnumHelper.MergeFlag(mergeEnum1, mergeEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void MergeFlag_NoneWithOne_MergedToOne()
            {
                var expectedResult = TestFlagEnum.Quarter;
                var mergeEnum1 = TestFlagEnum.None;
                var mergeEnum2 = TestFlagEnum.Quarter;

                var result = (TestFlagEnum)(EnumHelper.MergeFlag(mergeEnum1, mergeEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void MergeFlag_NoneWithTwo_MergedToTwo()
            {
                var expectedResult = TestFlagEnum.Quarter | TestFlagEnum.Red;
                var mergeEnum1 = TestFlagEnum.None;
                var mergeEnum2 = TestFlagEnum.Quarter | TestFlagEnum.Red;

                var result = (TestFlagEnum)(EnumHelper.MergeFlag(mergeEnum1, mergeEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void MergeFlag_OneWithTwoAndNone_MergedToThree()
            {
                var expectedResult = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green;
                var mergeEnum1 = TestFlagEnum.Quarter;
                var mergeEnum2 = TestFlagEnum.Red | TestFlagEnum.Green | TestFlagEnum.None;

                var result = (TestFlagEnum)(EnumHelper.MergeFlag(mergeEnum1, mergeEnum2));

                Assert.Equal(expectedResult, result);
            }

            // Value Tests

            [Fact]
            public void MergeFlag_OneWithOne_MergedToTwo()
            {
                var expectedResult = TestFlagEnum.Quarter | TestFlagEnum.Red;
                var mergeEnum1 = TestFlagEnum.Quarter;
                var mergeEnum2 = TestFlagEnum.Red;

                var result = (TestFlagEnum)(EnumHelper.MergeFlag(mergeEnum1, mergeEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void MergeFlag_OneWithThree_MergedToFour()
            {
                var expectedResult = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Green | TestFlagEnum.Blue;
                var mergeEnum1 = TestFlagEnum.Quarter;
                var mergeEnum2 = TestFlagEnum.Red | TestFlagEnum.Green | TestFlagEnum.Blue;

                var result = (TestFlagEnum)(EnumHelper.MergeFlag(mergeEnum1, mergeEnum2));

                Assert.Equal(expectedResult, result);
            }

            // Value With Number Tests

            [Fact]
            public void MergeFlag_OneWithNumber67_MergedToValue71()
            {
                var expectedResult = (TestFlagEnum)71;
                var mergeEnum1 = TestFlagEnum.Quarter;
                var mergeEnum2 = (TestFlagEnum)67;

                var result = (TestFlagEnum)(EnumHelper.MergeFlag(mergeEnum1, mergeEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void MergeFlag_OneWithNumber68_MergedToValue68()
            {
                var expectedResult = (TestFlagEnum)68;
                var mergeEnum1 = TestFlagEnum.Quarter;
                var mergeEnum2 = (TestFlagEnum)68;

                var result = (TestFlagEnum)(EnumHelper.MergeFlag(mergeEnum1, mergeEnum2));

                Assert.Equal(expectedResult, result);
            }
        }

        
        public class RemoveFlagMethod
        {
            // None Value Tests

            [Fact]
            public void RemoveFlag_NoneWithNone_ReturnNone()
            {
                var expectedResult = TestFlagEnum.None;
                var leftEnum1 = TestFlagEnum.None;
                var rightEnum2 = TestFlagEnum.None;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_NoneWithOne_ReturnNone()
            {
                var expectedResult = TestFlagEnum.None;
                var leftEnum1 = TestFlagEnum.None;
                var rightEnum2 = TestFlagEnum.Circle;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_NoneWithThree_ReturnNone()
            {
                var expectedResult = TestFlagEnum.None;
                var leftEnum1 = TestFlagEnum.None;
                var rightEnum2 = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_OneWithNone_ReturnOne()
            {
                var expectedResult = TestFlagEnum.Circle;
                var leftEnum1 = TestFlagEnum.Circle;
                var rightEnum2 = TestFlagEnum.None;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_ThreeWithNone_ReturnThree()
            {
                var expectedResult = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue;
                var leftEnum1 = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue;
                var rightEnum2 = TestFlagEnum.None;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            // Value Tests

            [Fact]
            public void RemoveFlag_OneWithOtherOne_ReturnOne()
            {
                var expectedResult = TestFlagEnum.Circle;
                var leftEnum1 = TestFlagEnum.Circle;
                var rightEnum2 = TestFlagEnum.Quarter;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_OneWithSameOne_ReturnNone()
            {
                var expectedResult = TestFlagEnum.None;
                var leftEnum1 = TestFlagEnum.Circle;
                var rightEnum2 = TestFlagEnum.Circle;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_OneWithOtherThree_ReturnOne()
            {
                var expectedResult = TestFlagEnum.Circle;
                var leftEnum1 = TestFlagEnum.Circle;
                var rightEnum2 = TestFlagEnum.Quarter | TestFlagEnum.Red | TestFlagEnum.Blue;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_OneWithOtherTwoAndSameOne_ReturnNone()
            {
                var expectedResult = TestFlagEnum.None;
                var leftEnum1 = TestFlagEnum.Circle;
                var rightEnum2 = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_ThreeWithSameOneAndOtherTwo_ReturnTwo()
            {
                var expectedResult = TestFlagEnum.Red | TestFlagEnum.Blue;
                var leftEnum1 = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue;
                var rightEnum2 = TestFlagEnum.Circle | TestFlagEnum.Quarter | TestFlagEnum.Green;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_ThreeWithSameTwoAndOtherOne_ReturnOne()
            {
                var expectedResult = TestFlagEnum.Blue;
                var leftEnum1 = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue;
                var rightEnum2 = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Green;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void RemoveFlag_ThreeWithSameThree_ReturnNone()
            {
                var expectedResult = TestFlagEnum.None;
                var leftEnum1 = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue;
                var rightEnum2 = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue;

                var result = (TestFlagEnum)(EnumHelper.RemoveFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }
        }

        
        public class InvertFlagMethod
        {
            // None Value Tests

            [Fact]
            public void InvertFlag_NoneWithNone_ReturnNone()
            {
                var expectedResult = TestFlagEnum.None;
                var leftEnum1 = TestFlagEnum.None;
                var rightEnum2 = TestFlagEnum.None;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void InvertFlag_OneWithNone_ReturnOne()
            {
                var expectedResult = TestFlagEnum.Circle;
                var leftEnum1 = TestFlagEnum.Circle;
                var rightEnum2 = TestFlagEnum.None;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void InvertFlag_NoneWithOne_ReturnOne()
            {
                var expectedResult = TestFlagEnum.Circle;
                var leftEnum1 = TestFlagEnum.None;
                var rightEnum2 = TestFlagEnum.Circle;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            // Value Tests

            [Fact]
            public void InvertFlag_OneWithSameOne_ReturnNone()
            {
                var expectedResult = TestFlagEnum.None;
                var leftEnum1 = TestFlagEnum.Circle;
                var rightEnum2 = TestFlagEnum.Circle;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void InvertFlag_OneWithOtherOne_ReturnTwo()
            {
                var expectedResult = TestFlagEnum.Circle | TestFlagEnum.Green;
                var leftEnum1 = TestFlagEnum.Circle;
                var rightEnum2 = TestFlagEnum.Green;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void InvertFlag_ThreeWithSameThree_ReturnNone()
            {
                var expectedResult = TestFlagEnum.None;
                var leftEnum1 = TestFlagEnum.Circle | TestFlagEnum.Green | TestFlagEnum.Red;
                var rightEnum2 = TestFlagEnum.Circle | TestFlagEnum.Green | TestFlagEnum.Red;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void InvertFlag_ThreeWithOtherThree_ReturnSix()
            {
                var expectedResult = TestFlagEnum.Circle | TestFlagEnum.Green | TestFlagEnum.Red | TestFlagEnum.Quarter | TestFlagEnum.Triangle | TestFlagEnum.Blue;
                var leftEnum1 = TestFlagEnum.Circle | TestFlagEnum.Green | TestFlagEnum.Red;
                var rightEnum2 = TestFlagEnum.Quarter | TestFlagEnum.Triangle | TestFlagEnum.Blue;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void InvertFlag_ThreeWithSameTwo_ReturnOne()
            {
                var expectedResult = TestFlagEnum.Red;
                var leftEnum1 = TestFlagEnum.Circle | TestFlagEnum.Green | TestFlagEnum.Red;
                var rightEnum2 = TestFlagEnum.Circle | TestFlagEnum.Green;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void InvertFlag_ThreeWithOtherTwo_ReturnFive()
            {
                var expectedResult = TestFlagEnum.Circle | TestFlagEnum.Green | TestFlagEnum.Red | TestFlagEnum.Quarter | TestFlagEnum.Blue;
                var leftEnum1 = TestFlagEnum.Circle | TestFlagEnum.Green | TestFlagEnum.Red;
                var rightEnum2 = TestFlagEnum.Quarter | TestFlagEnum.Blue;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }

            [Fact]
            public void InvertFlag_ThreeWithSameOneAndOtherTwo_ReturnFour()
            {
                var expectedResult = TestFlagEnum.Green | TestFlagEnum.Red | TestFlagEnum.Quarter | TestFlagEnum.Blue;
                var leftEnum1 = TestFlagEnum.Circle | TestFlagEnum.Green | TestFlagEnum.Red;
                var rightEnum2 = TestFlagEnum.Circle | TestFlagEnum.Quarter | TestFlagEnum.Blue;

                var result = (TestFlagEnum)(EnumHelper.InvertFlag(leftEnum1, rightEnum2));

                Assert.Equal(expectedResult, result);
            }
        }


        // Convert Enums

        
        public class ToNumber64Method
        {
            [Fact]
            public void ToNumber64_ZeroValue_Return0()
            {
                var expectedResult = 0;
                var enumToConvert = TestFlagEnum.None;

                var result = EnumHelper.ToNumber64(enumToConvert);

                Assert.Equal(expectedResult, result);
            }
            
            // TODO: Create more tests for ToNumber.
        }

        
        // public class ToListMethod
        // {
        //     [Fact]
        //     public void ToList_ZeroValue_ReturnOneItemList()
        //     {
        //         var expectedResult = new TestFlagEnum[] { TestFlagEnum.None };
        //         var enumList = EnumHelper.ToList(typeof(TestFlagEnum), TestFlagEnum.None).ToArray();

        //         CollectionAssert.AreEquivalent(expectedResult, enumList);
        //     }

        //     [Fact]
        //     public void ToList_SingleValue_ReturnOneItemList()
        //     {
        //         var expectedResult = new TestFlagEnum[] { TestFlagEnum.Circle };
        //         var enumList = EnumHelper.ToList(typeof(TestFlagEnum), TestFlagEnum.Circle).ToArray();

        //         CollectionAssert.AreEquivalent(expectedResult, enumList);
        //     }

        //     [Fact]
        //     public void ToList_ThreeValue_ReturnThreeItemList()
        //     {
        //         var expectedResult = new TestFlagEnum[] { TestFlagEnum.Circle, TestFlagEnum.Blue, TestFlagEnum.Red };
        //         var enumList = EnumHelper.ToList(typeof(TestFlagEnum), TestFlagEnum.Circle | TestFlagEnum.Blue | TestFlagEnum.Red).ToArray();

        //         CollectionAssert.AreEquivalent(expectedResult, enumList);
        //     }

        //     // Generic Pendant Tests

        //     [Fact]
        //     public void ToListGeneric_ThreeValue_ReturnThreeItemList()
        //     {
        //         var expectedResult = new TestFlagEnum[] { TestFlagEnum.Circle, TestFlagEnum.Blue, TestFlagEnum.Red };
        //         var enumList = EnumHelper.ToList<TestFlagEnum>(TestFlagEnum.Circle | TestFlagEnum.Blue | TestFlagEnum.Red).ToArray();

        //         CollectionAssert.AreEquivalent(expectedResult, enumList);
        //     }
        // }

        
        public class ToEnumMethod
        {
            // Check for exception Tests.

            [Fact]
            public void ToEnum_EmptyList_Exception()
            {
                var enumList = new TestFlagEnum[] { };
                try
                {
                    var result = EnumHelper.ToEnum(typeof(TestFlagEnum), enumList.Select(x => (Enum)(object)x).ToArray());
                    Assert.True(false, "Expected an Excpetion.");
                }
                catch (ArgumentException ex)
                {
                    Assert.Equal(ex.ParamName, "flags");
                }
                catch (Exception ex)
                {
                    Assert.True(false, string.Format("Wrong excpected Excpetion ({0}).", ex.GetType().Name));
                }
            }

            // Normal Use.

            [Fact]
            public void ToEnum_OneFlagList_OneFlagEnum()
            {
                var excpectedEnum = TestFlagEnum.Circle;
                var enumList = new TestFlagEnum[] { TestFlagEnum.Circle };

                var result = EnumHelper.ToEnum<TestFlagEnum>(enumList);

                Assert.Equal(excpectedEnum, result);
            }

            [Fact]
            public void ToEnum_TwoFlagList_TwoFlagEnum()
            {
                var excpectedEnum = TestFlagEnum.Circle | TestFlagEnum.Red;
                var enumList = new TestFlagEnum[] { TestFlagEnum.Circle, TestFlagEnum.Red };

                var result = EnumHelper.ToEnum<TestFlagEnum>(enumList);

                Assert.Equal(excpectedEnum, result);
            }

            [Fact]
            public void ToEnum_FourFlagList_FourFlagEnum()
            {
                var excpectedEnum = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue | TestFlagEnum.Green;
                var enumList = new TestFlagEnum[] { TestFlagEnum.Circle, TestFlagEnum.Red, TestFlagEnum.Blue, TestFlagEnum.Green };

                var result = EnumHelper.ToEnum<TestFlagEnum>(enumList);

                Assert.Equal(excpectedEnum, result);
            }

            [Fact]
            public void ToEnum_TwoItemsListWithTwoFlaggedEntries_FourFlagEnum()
            {
                var excpectedEnum = TestFlagEnum.Circle | TestFlagEnum.Red | TestFlagEnum.Blue | TestFlagEnum.Green;
                var enumList = new TestFlagEnum[] { TestFlagEnum.Circle | TestFlagEnum.Red, TestFlagEnum.Blue | TestFlagEnum.Green };

                var result = EnumHelper.ToEnum<TestFlagEnum>(enumList);

                Assert.Equal(excpectedEnum, result);
            }
        }
    }
}