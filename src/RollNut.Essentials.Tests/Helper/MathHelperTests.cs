using RollNut.Essentials.Helper;
using Xunit;

namespace RollNut.Essentials.Tests.Helper
{
    public class MathHelperTests
    {
        [Theory]
        [InlineData(5, 5d)]
        [InlineData(2d, 1d, 2d, 5d, 2d, 1d)]
        [InlineData(2.5, 2d, 5d, 3d, 1d)]
        public void Median(double expected, params double[] given)
        {
            var actual = MathHelper.Median(given);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(int.MaxValue, 1)]
        [InlineData(1, int.MaxValue)]
        public void AddingWillOverflow_True(int x, int y)
        {
            var actual = MathHelper.AddingWillOverflow(x, y);
            Assert.True(actual);
        }

        [Theory]
        [InlineData(int.MaxValue - 1, 1)]
        [InlineData(1, int.MaxValue - 1)]
        [InlineData(1, 32)]
        [InlineData(int.MinValue, int.MaxValue)]
        [InlineData(int.MinValue, 132)]
        public void AddingWillOverflow_False(int x, int y)
        {
            var actual = MathHelper.AddingWillOverflow(x, y);
            Assert.False(actual);
        }
    }
}